Feature: To test the registration of a simple Task and SubTask

  Background:
    Given I am on avenue code qa test site


  Scenario Outline: To create a user
    Given I SignIn on application with user "<email>" and password "<password>"
    When I add a task "<taskname>"
    When the task should contain  "<taskname>"
    When I add a subtask
    When I add a new subtask with name "<subTaskName>" and dueDate "<DueDate>"

    Examples:
      | email                     | password | taskname  | subTaskName | DueDate |
      | anderkazuhiro@hotmail.com | anderson | taskname1 | subTaskName | DueDate |
