package org.cucumber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyTasksPage extends ParentPage {

	public MyTasksPage(WebDriver driver) {
		super(driver);
	}


	public MyTasksPage TaskNameField(String taskName) {
		driver.findElement(By.id("new_task")).sendKeys("taskName");
		return this;
	}
	
	public MyTasksPage AddTaskBtn() {
		driver.findElement(By.xpath("//input[@id='new_task']//following::span")).click();
		return this;
	}
	
	public MyTasksPage addTask(String taskName) {
		this.TaskNameField(taskName)
		 	.AddTaskBtn();
		return this;
	}

	public MyTasksPage removeTask() {
		driver.findElement(By.xpath("//button[text()='Remove'][1]")).click();
		return this;
	}


	public ManageSubTaskPopup clickManageSubtask() {
	driver.findElement(By.xpath("//button[contains(text(),\"Manage Subtasks\")]")).click();
	return new ManageSubTaskPopup(driver);
}
	public String VerifyTextFirstTask() {
		String value = driver.findElement(By.xpath("//div[@class='panel panel-default']//tr[1]//td[2]")).getText();
		return value;
}
	


}

