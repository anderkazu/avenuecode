package org.cucumber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage extends ParentPage {

	public SignInPage(WebDriver driver) {
		super(driver);
	}
	
	public SignInPage PreencherEmail(String Email) {
		driver.findElement(By.id("user_email")).sendKeys(Email);
		return this;
			
	}
	public SignInPage PreencherSenha(String Senha) {
		driver.findElement(By.id("user_password")).sendKeys(Senha);
		return this;
			
	}
	
	public HomePage SignIn() {
		driver.findElement(By.name("commit")).click();
		return new HomePage(driver);
			
	}
	
	public MyTasksPage SignInApplication(String Email, String Senha) {
		
		this.PreencherEmail(Email)
			.PreencherSenha(Senha)
			.SignIn();
		return new MyTasksPage(driver);

		
	}
}
