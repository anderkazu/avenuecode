package org.cucumber.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManageSubTaskPopup extends ParentPage {

	public ManageSubTaskPopup(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


 
 public ManageSubTaskPopup fullfillSubTaskDescription(String SubTaskName) {
	 driver.findElement(By.id("new_sub_task")).sendKeys(SubTaskName);
		return this;
 }
 
 public ManageSubTaskPopup fullfillSubTaskDueData(String dueDate) {
	 driver.findElement(By.id("dueDate")).clear();
	 driver.findElement(By.id("dueDate")).sendKeys(dueDate);
	return this;
 }

 public ManageSubTaskPopup addSubTaskBtn() {
	 driver.findElement(By.id("add-subtask")).click();
	return this;
 }
 

 public ManageSubTaskPopup closeSubTaskBtn() {
	 driver.findElement(By.xpath("//div[@class='modal-content']//button[text()='Close']")).click();
	return this;
 }

 
 public ManageSubTaskPopup addNewSubTask(String SubTaskName,String dueDate) {
	 this.fullfillSubTaskDescription(SubTaskName)
	 	 .fullfillSubTaskDueData(dueDate)
	 	 .addSubTaskBtn()
	 	 .closeSubTaskBtn();
	 
	 return this;
 }
 
 
}