package org.cucumber.pages;

		import org.openqa.selenium.By;
		import org.openqa.selenium.WebDriver;

public class HomePage extends ParentPage{

	public HomePage(WebDriver driver) {

		super(driver);
	}

	public SignInPage clickSignIn() {
		driver.findElement(By.xpath("//input[@name='register']")).click();
		return new SignInPage(driver);

	}

	public MyTasksPage clickMyTasks() {
		driver.findElement(By.xpath("//input[@name='register']")).click();
		return new MyTasksPage(driver);

	}
}

