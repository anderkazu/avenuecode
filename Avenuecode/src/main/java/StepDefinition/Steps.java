package StepDefinition;

import cucumber.api.PendingException;
import org.cucumber.pages.HomePage;
import org.cucumber.pages.ManageSubTaskPopup;
import org.cucumber.pages.MyTasksPage;
//import org.cucumber.pages.RegisterConf;
//import org.cucumber.pages.RegisterPage;
import org.cucumber.pages.SignInPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.Assert;

import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {

	WebDriver driver;
	HomePage hmpage;
	SignInPage signInPage;
	MyTasksPage mytasksPage;
	ManageSubTaskPopup subTasksPopup;


	@Given("^I am on avenue code qa test site$")
	public void i_am_on_avenue_code_qa_test_site() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		hmpage = new HomePage(driver);
		hmpage.clickSignIn();

	}

	@When("^I SignIn on application with user \"([^\\\"]*)\\\" and password \"([^\"]*)\"$")
	public void SignInApplication(String user,String password) throws Throwable {
		signInPage = hmpage.clickSignIn();
	}


	@When("^I add a task \"([^\"]*)\"$")
	public void i_enter_a_task(String taskName) throws Throwable {
		mytasksPage.addTask(taskName);
	}

	@When("^the task should contain  \"([^\"]*)\"$")
	public void the_task_should_contain(String taskName) throws Throwable {
		String text1 = mytasksPage.VerifyTextFirstTask();
		Assert.assertTrue(text1.contains(taskName));
	}

	@When("^I add a subtask$")
	public void add_subtask() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		subTasksPopup = mytasksPage.clickManageSubtask();
	}

	@When("^I add a new subtask with name \"([^\\\"]*)\\\" and dueDate \"([^\"]*)\"$")
	public void addSubTask(String subTaskName,String dueDate) throws Throwable {
		subTasksPopup.addNewSubTask(subTaskName, dueDate);
	}


}
